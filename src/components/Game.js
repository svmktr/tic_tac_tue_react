import React, { Component } from 'react'

export class Game extends Component {
state = {
    turn : 'X',
    board : Array(9).fill(''),
    gameEnd : false,
    winner: undefined,
    totalMoves:  0,
    message: ''

    
}


eventHandler = (e) =>{
    // console.log(e.target.innerText)
    // console.log(e.target.dataset)
    if (this.state.gameEnd) return
   if(this.state.board[e.target.dataset.square] === ''){
    this.state.board[e.target.dataset.square] = this.state.turn
    e.target.innerText = this.state.turn
    this.setState({
        turn : this.state.turn === 'X' ? 'O' : 'X',
        board: this.state.board,
        totalMoves : this.state.totalMoves += 1
    })
}


//  console.log(this.state.board)
 let result = this.checkWinner()
 if(result === 'X'){
     this.setState({
    gameEnd : true,
    winner : 'X',
    message :'Winner is X'
})
 }
 else if(result === 'O')
 {
    this.setState({
        gameEnd : true,
        winner : 'O',
        message:'winner is O'
    })
 }

 else if(result === 'draw')
 {
    this.setState({
        gameEnd : true,
        winner : 'Draw',
        message:'Game draw'
    })
 }
}




checkWinner() {
    let wonMoves = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 4, 8], [2, 4, 6], [0, 3, 6], [1, 4, 7], [2, 5, 8]];
    let board = this.state.board
 
    for (let i=0; i<wonMoves.length; i++){
        if(board[wonMoves[i][0]] === board[wonMoves[i][1]]  && board[wonMoves[i][1]] === board[wonMoves[i][2]]  && board[wonMoves[i][0]] !== '' )
            return board[wonMoves[i][0]]
    }
    // console.log(this.state.totalMoves)
if(this.state.totalMoves === 9)
    return 'draw'

}


    render() {
        return (
            <div id='game'>
            <header className='header'>
                Tic_Tac_Toe
            </header>
        <div className='status'>{this.state.message}</div>
            <div id='board' onClick={(e)=>this.eventHandler(e)}>
                <div className="square" data-square ='0'></div>
                <div className="square" data-square='1'></div>
                <div className="square" data-square='2'></div>
                <div className="square" data-square='3'></div>
                <div className="square" data-square='4'></div>
                <div className="square" data-square='5'></div>
                <div className="square" data-square='6'></div>
                <div className="square" data-square='7'></div>
                <div className="square" data-square='8'></div>
            </div>
            </div>
        )
    }
}

export default Game
